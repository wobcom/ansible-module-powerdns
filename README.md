PowerDNS Ansible library
==========
- [Development](#development)
- [Introduction](#introduction)
- [Usage](#usage)
  - [Zones](#powerdns_zone)
  - [Records](#powerdns_rrset)


# Development
## Setting up a PowerDNS authoritative server for testing
For testing purposes this repository comes with a development VM running a PowerDNS authoritative instance. 
```bash
cd ansible/roles
git clone https://gitlab.berlin.ccc.de/fluepke/powerdns-authoritative.git
vagrant up
```

# Introduction
Manipulate zones and rrsets via the PowerDNS API.

# Requirements
 * [PowerdnsAPI](http://gitlab.service.wobcom.de/fluepke/powerdns-api)

# Usage
## powerdns_zone
Manipulate zones on the powerdns server

```yaml
options:
  account:
    description:
      - MAY be set.
      - Its value is defined by local policy
    required: False
    default: ''
    type: str
  api_rectify:
    description:
      - Whether or not the zone will be rectified on data changes via the API
    required: False
    default: True
    type: bool
  dnssec:
    description:
      - Whether or not this zone is DNSSEC signed
      - (inferred from presigned being true XOR presence of at least one cryptokey with active being true)
    required: False
    default: None
    type: bool
  kind:
    description:
      - Zone kind
    required: False
    default: 'Master'
    type: str
    choices: ['Master', 'Native', 'Slave']
  master_tsig_key_ids:
    description:
      - The id of the TSIG keys used for master operation in this zone
    required: False
    default: []
    type: list
  masters:
    description:
      - List of IP addresses configured as a master for this zone (“Slave” type zones only)
    required: False
    default: []
    type: list
  name:
    description:
      - Name of the zone (e.g. “example.com.”)
      - MUST have a trailing dot
    required: True
    type: str
  nsec3narrow:
    description:
      - Whether or not the zone uses NSEC3 narrow
    required: False
    default: False
    type: bool
  nsec3param:
    description:
      - The NSEC3PARAM record
    required: False
    default: ''
    type: str
  presigned:
    description:
      - Whether or not the zone is pre-signed
    required: False
    default: False
    type: bool
  serial:
    description:
      - The SOA serial number
    required: False
    default: None
    type: int
  slave_tsig_key_ids:
    description:
      - The id of the TSIG keys used for slave operation in this zone
    required: False
    default: []
    type: list
  soa_edit:
    description:
      - The SOA-EDIT metadata item
    required: False
    default: INCREMENT-WEEKS
    type: str
    choices:
      - INCREMENT-WEEKS
      - INCEPTION-EPOCH
      - INCEPTION-INCREMENT
      - EPOCH
      - NONE
  soa_edit_api:
    description:
      - The SOA-EDIT-API metadata item
    required: False
    default: EPOCH
    type: str
    choices:
      - INCREMENT-WEEKS
      - INCEPTION-EPOCH
      - INCEPTION-INCREMENT
      - EPOCH
      - NONE
  api_url:
    description:
      - PowerDNS API url
      - Example http://127.0.0.1:8053/api/v1
    required: True
    type: str
  api_secret:
    description:
      - PowerDNS API api
    required: True
    type: str
  server_id:
    description:
      - PowerdnsAPI server id
    required: False
    default: localhost
    type: str
  state:
    description:
      - Zone state
    required: False
    default: present
    type: str
    choices: ["present", "absent"]
```

### Examples
Create a zone named example.org
```yaml
- powerdns_zone:
    name: example.org
    api_url: 'http://127.0.0.1:8053/api/v1'
    api_secret: changeme
```



## powerdns_rrset
Create, update or delete a Resource Record Set (all records with the same name and type)

```yaml
options:
  comments:
    description:
      - List of Comment.
      - Must be empty when changetype is set to DELETE. An empty list results in deletion of all comments. modified_at is optional and defaults to the current server time.
    required: False
    default: []  
  name:
    description:
      - Name for record set (e.g. “www.powerdns.com.”)
    required: True
    type: str
  records:
    description:
      - All records in this RRSet. When updating Records, this is the list of new records (replacing the old ones). Must be empty when changetype is set to DELETE.
      - An empty list results in deletion of all records (and comments).
    required: False
    default: []
    type: list
  set_ptr:
    description:
      - If set to true, the server will find the matching reverse zone and create a PTR there.
      - Existing PTR records are replaced. If no matching reverse Zone, an error is thrown.
    required: False
    default: False
    type: bool
  ttl:
    description:
      - DNS TTL of the records, in seconds. MUST NOT be included when changetype is set to “DELETE”.
    required: False
    default: 3600
    type: int
  type:
    description:
      - Type of this record (e.g. “A”, “PTR”, “MX”)
    required: False
    default: A
    type: str
    choices: ["A", "AAAA", "AFSDB", "ALIAS", "CAA", "CERT", "CDNSKEY", "CDS", "CNAME", "DNSKEY", "DNAME", "DS", "HINFO", "KEY", "LOC", "MX", "NAPTR", "NS", "NSEC", "NSEC3", "NSEC3PARAM", "OPENPGPKEY", "PTR", "RP", "RRSIG", "SOA", "SPF", "SSHFP", "SRV", "TKEY", "TSIG", "TLSA", "SMIMEA", "TXT", "URI"]
  api_url:
    description:
      - PowerDNS API url
      - Example http://127.0.0.1:8053/api/v1
    required: True
    type: str
  api_secret:
    description:
      - PowerDNS API api
    required: True
    type: str
  server_id:
    description:
      - PowerdnsAPI server id
    required: False
    default: localhost
    type: str
  zone_id:
    description:
      - PowerDNS zone id (e.g. "example.org.")
    required: True
    required: True
    type: str
  state:
    description:
      - Zone state
    required: False
    default: present
    type: str
    choices: ["present", "absent"]
```

### Examples
```yaml
- powerdns_record:
    name: evil.example.org.
    type: A
    records:
      - 1.1.1.1
      - 2.2.2.2
    api_url: "192.168.78.20:8053/api/v1"
    api_secret: changeme
    zone_id: example.org
```

