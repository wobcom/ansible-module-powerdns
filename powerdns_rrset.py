#!/usr/bin/env python
# -*- coding: utf-8 -*-
import PowerdnsAPI
from PowerdnsAPI.rest import ApiException

DOCUMENTATION = '''
---
module: powerdns_rrset
short_description: Manage PowerDNS rrsets
description:
  - Create, update or delete a Resource Record Set (all records with the same name and type)
  - see https://doc.powerdns.com/authoritative/http-api/zone.html#rrset
author: Fabian Luepke (@fluepke)
options:
  comments:
    description:
      - List of Comment.
      - Must be empty when changetype is set to DELETE. An empty list results in deletion of all comments. modified_at is optional and defaults to the current server time.
    required: False
    default: []  
  name:
    description:
      - Name for record set (e.g. “www.powerdns.com.”)
    required: True
    type: str
  records:
    description:
      - All records in this RRSet. When updating Records, this is the list of new records (replacing the old ones). Must be empty when changetype is set to DELETE.
      - An empty list results in deletion of all records (and comments).
    required: False
    default: []
    type: list
  set_ptr:
    description:
      - If set to true, the server will find the matching reverse zone and create a PTR there.
      - Existing PTR records are replaced. If no matching reverse Zone, an error is thrown.
    required: False
    default: False
    type: bool
  ttl:
    description:
      - DNS TTL of the records, in seconds. MUST NOT be included when changetype is set to “DELETE”.
    required: False
    default: 3600
    type: int
  type:
    description:
      - Type of this record (e.g. “A”, “PTR”, “MX”)
    required: False
    default: A
    type: str
    choices: ["A", "AAAA", "AFSDB", "ALIAS", "CAA", "CERT", "CDNSKEY", "CDS", "CNAME", "DNSKEY", "DNAME", "DS", "HINFO", "KEY", "LOC", "MX", "NAPTR", "NS", "NSEC", "NSEC3", "NSEC3PARAM", "OPENPGPKEY", "PTR", "RP", "RRSIG", "SOA", "SPF", "SSHFP", "SRV", "TKEY", "TSIG", "TLSA", "SMIMEA", "TXT", "URI"]
  api_url:
    description:
      - PowerDNS API url
      - Example http://127.0.0.1:8053/api/v1
    required: True
    type: str
  api_secret:
    description:
      - PowerDNS API api
    required: True
    type: str
  server_id:
    description:
      - PowerdnsAPI server id
    required: False
    default: localhost
    type: str
  zone_id:
    description:
      - PowerDNS zone id (e.g. "example.org.")
    required: True
    required: True
    type: str
  state:
    description:
      - Zone state
    required: False
    default: present
    type: str
    choices: ["present", "absent"]
'''

EXAMPLES = '''
'''

import PowerdnsAPI

def convert_records(records, set_ptr):
  return [PowerdnsAPI.Record(
    content=record,
    disabled=False,
    set_ptr=set_ptr) for record in records]

def compare(rrset1, rrset2):
  records1 = [dict(content=record.content, disabled=record.disabled or False) for record in rrset1.records]
  records2 = [dict(content=record.content, disabled=record.disabled or False) for record in rrset2.records]
  records1.sort(key=lambda record: record['content'])
  records2.sort(key=lambda record: record['content'])

  return (records1 == records2
    and rrset1.ttl == rrset2.ttl)

def main():
    module = AnsibleModule(
        argument_spec = {
            'comments': {'type': 'list', 'required': False, 'default': []},
            'name': {'type': 'str', 'required': True},
            'records': {'type': 'list', 'required': False, 'default': []},
            'set_ptr': {'type': 'bool', 'required': False, 'default': False},
            'ttl': {'type': 'int', 'required': False, 'default': 3600},
            'type': {'type': 'str', 'required': False, 'default': 'A', 'choices': ['A', 'AAAA', 'AFSDB', 'ALIAS', 'CAA', 'CERT', 'CDNSKEY',
              'CDS', 'CNAME', 'DNSKEY', 'DNAME', 'DS', 'HINFO', 'KEY', 'LOC', 'MX', 'NAPTR', 'NS', 'NSEC', 'NSEC3', 'NSEC3PARAM',
              'OPENPGPKEY', 'PTR', 'RP', 'RRSIG', 'SOA', 'SPF', 'SSHFP', 'SRV', 'TKEY', 'TSIG', 'TLSA', 'SMIMEA', 'TXT', 'URI']},
            'api_url': {'type': 'str', 'required': True},
            'api_secret': {'type': 'str', 'required': True},
            'server_id': {'type': 'str', 'required': False, 'default': 'localhost'},
            'zone_id': {'type': 'str', 'required': True},
            'state': {'type': 'str', 'required': False, 'default': 'present', 'choices': ['present', 'absent']}
        },
        supports_check_mode=True,
    )

    comments = module.params['comments']
    name = module.params['name'].lower()
    records = convert_records(module.params['records'], module.params['set_ptr'])
    ttl = module.params['ttl']
    type = module.params['type']
    api_url = module.params['api_url']
    api_secret = module.params['api_secret']
    server_id = module.params['server_id']
    zone_id = module.params['zone_id'].lower()
    state = module.params['state']

    configuration = PowerdnsAPI.Configuration()
    configuration.host = api_url
    configuration.api_key['X-API-Key'] = api_secret

    api_client = PowerdnsAPI.ZonesApi(PowerdnsAPI.ApiClient(configuration))

    new_rrset = PowerdnsAPI.RRSet(
      changetype='REPLACE',
      comments=comments,
      name=name,
      records=records,
      ttl=ttl,
      type=type)

    try:
      zone = api_client.list_zone(server_id, zone_id) 
    except ApiException as e:
      if e.status == 404:
        raise ValueError("Zone must exist prior to configuring RRSets")
      else:
        print("Exception when calling ConfigApi->get_config: %s\n" % e)

    existing_rrset = None
    for rrset in zone.rrsets:
      if rrset.name == name:
        existing_rrset = rrset

    # if zone does not exist and we do not want it to exist, we are done
    if not existing_rrset and state == 'absent':
      module.exit_json(changed=False)

    if not existing_rrset and state == 'present':
      zone.rrsets = [new_rrset]
      if not module.check_mode:
        api_client.patch_zone(server_id, zone_id, zone)
      module.exit_json(changed=True)

    if existing_rrset and state == 'absent':
      if not module.check_mode:
        new_rrset.changetype='DELETE'
        zone.rrsets = [new_rrset]
        api_client.patch_zone(server_id, zone_id, zone)
      module.exit_json(changed=True)

    if existing_rrset and state == 'present' and not compare(existing_rrset, new_rrset):
      if not module.check_mode:
        zone.rrsets = [new_rrset]
        api_client.patch_zone(server_id, zone_id, zone)
      module.exit_json(changed=True)
    module.exit_json(changed=False)

# import module snippets
from ansible.module_utils.basic import *

if __name__ == '__main__':
    main()
