#/!/usr/bin/env python
# -*- coding: utf-8 -*-

from ansible.module_utils.basic import *
try:
  import PowerdnsAPI
  from PowerdnsAPI.rest import ApiException
except ImportError:
  module.fail_json(message='Please install PowerdnsAPI first')

DOCUMENTATION = '''
---
module: powerdns_zone
short_description: Manage PowerDNS zones
description:
  - Create, update or delete a PowerDNS zone using API
  - see https://doc.powerdns.com/authoritative/http-api/zone.html#zone
author: Fabian Luepke (@fluepke)
options:
  account:
    description:
      - MAY be set.
      - Its value is defined by local policy
    required: False
    default: ''
    type: str
  api_rectify:
    description:
      - Whether or not the zone will be rectified on data changes via the API
    required: False
    default: True
    type: bool
  dnssec:
    description:
      - Whether or not this zone is DNSSEC signed
      - (inferred from presigned being true XOR presence of at least one cryptokey with active being true)
    required: False
    default: None
    type: bool
  kind:
    description:
      - Zone kind
    required: False
    default: 'Master'
    type: str
    choices: ['Master', 'Native', 'Slave']
  master_tsig_key_ids:
    description:
      - The id of the TSIG keys used for master operation in this zone
    required: False
    default: []
    type: list
  masters:
    description:
      - List of IP addresses configured as a master for this zone (“Slave” type zones only)
    required: False
    default: []
    type: list
  name:
    description:
      - Name of the zone (e.g. “example.com.”)
      - MUST have a trailing dot
    required: True
    type: str
  nsec3narrow:
    description:
      - Whether or not the zone uses NSEC3 narrow
    required: False
    default: False
    type: bool
  nsec3param:
    description:
      - The NSEC3PARAM record
    required: False
    default: ''
    type: str
  presigned:
    description:
      - Whether or not the zone is pre-signed
    required: False
    default: False
    type: bool
  serial:
    description:
      - The SOA serial number
    required: False
    default: None
    type: int
  slave_tsig_key_ids:
    description:
      - The id of the TSIG keys used for slave operation in this zone
    required: False
    default: []
    type: list
  soa_edit:
    description:
      - The SOA-EDIT metadata item
    required: False
    default: INCREMENT-WEEKS
    type: str
    choices:
      - INCREMENT-WEEKS
      - INCEPTION-EPOCH
      - INCEPTION-INCREMENT
      - EPOCH
      - NONE
  soa_edit_api:
    description:
      - The SOA-EDIT-API metadata item
    required: False
    default: EPOCH
    type: str
    choices:
      - INCREMENT-WEEKS
      - INCEPTION-EPOCH
      - INCEPTION-INCREMENT
      - EPOCH
      - NONE
  api_url:
    description:
      - PowerDNS API url
      - Example http://127.0.0.1:8053/api/v1
    required: True
    type: str
  api_secret:
    description:
      - PowerDNS API api
    required: True
    type: str
  server_id:
    description:
      - PowerdnsAPI server id
    required: False
    default: localhost
    type: str
  state:
    description:
      - Zone state
    required: False
    default: present
    type: str
    choices: ["present", "absent"]
'''

EXAMPLES = '''
'''


def compare(zone1, zone2):
  return (zone1.account == zone2.account
    and zone1.api_rectify == zone2.api_rectify
    and zone1.kind == zone2.kind
    and zone1.master_tsig_key_ids == zone2.master_tsig_key_ids
    and zone1.masters == zone2.masters
    and zone1.nsec3narrow == zone2.nsec3narrow
    and zone1.nsec3param == zone2.nsec3param
    and zone1.presigned == zone2.presigned
    and zone1.slave_tsig_key_ids == zone2.slave_tsig_key_ids
    and zone1.soa_edit == zone2.soa_edit
    and zone1.soa_edit_api == zone2.soa_edit_api)


def set_changetype(rrset, changetype="REPLACE"):
  rrset.changetype = changetype
  return rrset

def main():
    module = AnsibleModule(
        argument_spec={
            'account': {'type': 'str', 'required': False, 'default': ''},
            'api_rectify': {'type': 'bool', 'required': False, 'default': True},
            'dnssec': {'type': 'bool', 'required': False, 'default': False},
            'kind': {'type': 'str', 'required': False, 'default': 'Master', 'choices': ['Master', 'Native', 'Slave']},
            'master_tsig_key_ids': {'type': 'list', 'required': False, 'default': []},
            'masters': {'type': 'list', 'required': False, 'default': []},
            'name': {'type': 'str', 'required': True},
            'nsec3narrow': {'type': 'bool', 'required': False, 'default': False},
            'nsec3param': {'type': 'str', 'required': False, 'default': ''},
            'presigned': {'type': 'bool', 'required': False, 'default': None},
            'serial': {'type': 'int', 'required': False, 'default': None},
            'slave_tsig_key_ids': {'type': 'list', 'required': False, 'default': []},
            'soa_edit': {'type': 'str', 'required': False, 'default': 'INCREMENT-WEEKS', 'choices': [
              'INCREMENT-WEEKS', 'INCEPTION-EPOCH', 'INCEPTION-INCREMENT', 'EPOCH', 'NONE']},
            'soa_edit_api': {'type': 'str', 'required': False, 'default': 'EPOCH', 'choices': [
              'INCREMENT-WEEKS', 'INCEPTION-EPOCH', 'INCEPTION-INCREMENT', 'EPOCH', 'NONE']},
            'api_url': {'type': 'str', 'required': True},
            'api_secret': {'type': 'str', 'required': True},
            'server_id': {'type': 'str', 'required': False, 'default': 'localhost'},
            'state': {'type': 'str', 'required': False, 'default': 'present', 'choices': ['present', 'absent']}
        },
        supports_check_mode=True,
    )

    account = module.params['account']
    api_rectify = module.params['api_rectify']
    dnssec = module.params['dnssec']
    kind = module.params['kind']
    master_tsig_key_ids = module.params['master_tsig_key_ids']
    masters = module.params['masters']
    name = module.params['name']
    nsec3narrow = module.params['nsec3narrow']
    nsec3param = module.params['nsec3param']
    presigned = module.params['presigned']
    serial = module.params['serial']
    slave_tsig_key_ids = module.params['slave_tsig_key_ids']
    soa_edit = module.params['soa_edit']
    soa_edit_api = module.params['soa_edit_api']
    api_url = module.params['api_url']
    api_secret = module.params['api_secret']
    server_id = module.params['server_id']
    state = module.params['state']

    configuration = PowerdnsAPI.Configuration()
    configuration.host = api_url
    configuration.api_key['X-API-Key'] = api_secret

    api_client = PowerdnsAPI.ZonesApi(PowerdnsAPI.ApiClient(configuration))

    new_zone = PowerdnsAPI.Zone()
    new_zone.account = account
    new_zone.api_rectify = api_rectify
    new_zone.dnssec = dnssec
    new_zone.kind = kind
    new_zone.master_tsig_key_ids = master_tsig_key_ids
    new_zone.name = name
    new_zone.nameservers = []
    new_zone.masters = masters
    new_zone.nsec3narrow = nsec3narrow
    new_zone.nsec3param = nsec3param
    new_zone.presigned = presigned
    new_zone.serial = serial
    new_zone.slave_tsig_key_ids = slave_tsig_key_ids
    new_zone.soa_edit = soa_edit
    new_zone.soa_edit_api = soa_edit_api

    changed = False

    try:
      existing_zone = api_client.list_zone(server_id, name) 
    except ApiException as e:
      if e.status == 404:
        existing_zone = None
      else:
        print("Exception when calling ConfigApi->get_config: %s\n" % e)

    if not existing_zone and state == 'absent':
      changed = False

    if not existing_zone and state == 'present':
      if not module.check_mode:
        api_client.create_zone(server_id, new_zone)
      changed = True

    if existing_zone and state == 'absent':
      if not module.check_mode:
        api_client.delete_zone(server_id, name)
      changed = True

    if existing_zone and state == 'present' and not compare(existing_zone, new_zone):
      new_zone.rrsets = [set_changetype(rrset) for rrset in existing_zone.rrsets]
      if not module.check_mode:
        api_client.patch_zone(server_id, name, new_zone)
      changed = True
    else:
      changed = False

    zone = api_client.list_zone(server_id, name)
    result = {
      'changed': changed,
      'zone': zone.to_dict()
    }
    module.exit_json(**result)

# import module snippets

if __name__ == '__main__':
    main()
